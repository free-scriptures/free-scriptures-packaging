#!/bin/sh
# Copyright (C) 2017-2019 Stephan Kreutzer
#
# This file is part of free-scriptures-packaging.
#
# free-scriptures-packaging is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3 or any later version,
# as published by the Free Software Foundation.
#
# free-scriptures-packaging is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with free-scriptures-packaging. If not, see <http://www.gnu.org/licenses/>.

rm -rf .git
git clone https://gitlab.com/free-scriptures/free-scriptures.git
cd free-scriptures
rm -rf .git
cd tools
# Consider the JDK version (target framework), as versions are forward compatible,
# but not backward compatible!
make
cd ..
cd ..

printf "Build date: $(date "+%Y-%m-%d").\n" > ./free-scriptures/version.txt
currentDate=$(date "+%Y%m%d")

cp -r free-scriptures free-scriptures_gnu
cp -r -n scripts_gnu/* free-scriptures_gnu
zip -r free-scriptures_gnu_$currentDate.zip free-scriptures_gnu
sha256sum free-scriptures_gnu_$currentDate.zip > free-scriptures_gnu_$currentDate.zip.sha256
rm -r free-scriptures_gnu

cp -r free-scriptures free-scriptures_windows
cp -r -n scripts_windows/* free-scriptures_windows
zip -r free-scriptures_windows_$currentDate.zip free-scriptures_windows
sha256sum free-scriptures_windows_$currentDate.zip > free-scriptures_windows_$currentDate.zip.sha256
rm -r free-scriptures_windows

rm -r free-scriptures
